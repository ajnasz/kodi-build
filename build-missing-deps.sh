#!/bin/bash

set -eo pipefail

cd ../kodi

if [ $# -ne 1 ];then
	echo "Build name must be set" &>2
	exit 1
fi

BUILD_NAME=$(echo ${1:=Matrix} | tr '[:upper:]' '[:lower:]')

if [ "$BUILD_NAME" = "leia" ]; then
	HASH="Leia"
elif [ "$BUILD_NAME" = "matrix" ];then
	HASH="Matrix"
else
	echo "Not a valid version" &>2
	exit 1
fi

echo "Configuring $BUILD_NAME"

cd ../kodi/
git checkout $HASH
# git checkout -- tools/depends/target
cd -

sudo make -C tools/depends/target/waylandpp PREFIX=/usr/local/kodi-$BUILD_NAME
sudo make -C tools/depends/target/wayland-protocols PREFIX=/usr/local/kodi-$BUILD_NAME AUTORECONF=/usr/bin/autoreconf
# sudo make -C tools/depends/target/wayland PREFIX=/usr/local/kodi-$BUILD_NAME
sudo make -C tools/depends/target/crossguid PREFIX=/usr/local/kodi-$BUILD_NAME
sudo make -C tools/depends/target/flatbuffers PREFIX=/usr/local/kodi-$BUILD_NAME
sudo make -C tools/depends/target/libfmt PREFIX=/usr/local/kodi-$BUILD_NAME
sudo make -C tools/depends/target/rapidjson PREFIX=/usr/local/kodi-$BUILD_NAME
sudo make -C tools/depends/target/libspdlog PREFIX=/usr/local/kodi-$BUILD_NAME
