#!/bin/bash

set -eo pipefail

SOURCE_DIR="$( cd "$( dirname "$0" )" && pwd )/../inputstream.adaptive"
cd $SOURCE_DIR && mkdir -p build && cd build
cmake -DDOWNLOAD_EXTRACT_TIMESTAMP=new -DADDONS_TO_BUILD=inputstream.adaptive -DADDON_SRC_PREFIX=$SOURCE_DIR/.. -DCMAKE_INSTALL_PREFIX=$SOURCE_DIR/../kodi/addons -DPACKAGE_ZIP=1 $SOURCE_DIR/../kodi/cmake/addons
make

cd $SOURCE_DIR/../kodi/addons
pwd
DEST=$HOME/inputstream.adaptive.zip
zip --symlinks -r $DEST inputstream.adaptive
echo "Inputstream adaptive built to $DEST"
