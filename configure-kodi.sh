#!/bin/sh

if [ $# -ne 1 ];then
	echo "Build name must be set" &>2
	exit 1
fi

BUILD_NAME=$(echo ${1:=Nexus} | tr '[:upper:]' '[:lower:]')

if [ "$BUILD_NAME" = "omega" ];then
	HASH="Omega"
elif [ "$BUILD_NAME" = "nexus" ];then
	HASH="Nexus"
elif [ "$BUILD_NAME" = "leia" ]; then
	HASH="Leia"
elif [ "$BUILD_NAME" = "matrix" ];then
	HASH="Matrix"
else
	echo "Not a valid version" &>2
	exit 1
fi

echo "Configuring $BUILD_NAME"

cd ../kodi/
git checkout $HASH
cd -


rm -rf CMakeCache.txt CMakeFiles/ CTestTestfile.cmake Makefile addons/ build/ cmake_install.cmake kodi-x11 kodi-xrandr launch-c launch-cxx libkodi.a media/ system/ userdata/ xbmc/

if [ "$HASH" = "Omega" ];then
	cmake ../kodi \
		-DCMAKE_INSTALL_PREFIX="/usr/local/kodi-$BUILD_NAME" \
		-DENABLE_INTERNAL_FLATBUFFERS=ON \
		-DENABLE_INTERNAL_FMT=ON \
		-DENABLE_INTERNAL_CROSSGUID=ON \
		-DENABLE_VAAPI=ON \
		-DAPP_RENDER_SYSTEM=gl \
		-DENABLE_INTERNAL_SPDLOG=ON \
		-DCORE_PLATFORM_NAME=x11
elif [ "$HASH" = "Nexus" ];then
	cmake ../kodi \
		-DCMAKE_INSTALL_PREFIX="/usr/local/kodi-$BUILD_NAME" \
		-DENABLE_INTERNAL_FLATBUFFERS=ON \
		-DENABLE_INTERNAL_FMT=ON \
		-DENABLE_INTERNAL_CROSSGUID=ON \
		-DENABLE_VAAPI=ON \
		-DAPP_RENDER_SYSTEM=gl \
		-DENABLE_INTERNAL_SPDLOG=ON \
		-DCORE_PLATFORM_NAME=x11
elif [ "$HASH" = "Matrix" ];then
	cmake ../kodi -DCMAKE_INSTALL_PREFIX=/usr/local/kodi-$BUILD_NAME -DENABLE_INTERNAL_FLATBUFFERS=ON -DENABLE_INTERNAL_FMT=ON -DENABLE_INTERNAL_CROSSGUID=ON -DENABLE_VAAPI=ON -DAPP_RENDER_SYSTEM=gl -DENABLE_INTERNAL_SPDLOG=ON -DCORE_PLATFORM_NAME=x11
else
	cmake ../kodi -DCMAKE_INSTALL_PREFIX=/usr/local/kodi-$BUILD_NAME -DENABLE_INTERNAL_FLATBUFFERS=ON -DENABLE_INTERNAL_FMT=ON -DENABLE_INTERNAL_CROSSGUID=ON -DENABLE_VAAPI=ON -DX11_RENDER_SYSTEM=gl
fi
