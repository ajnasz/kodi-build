Scripts which helps to build kodi on debian for a ~~Zotac ID40~~ Intel NUC.

VGA:
~~03:00.0 VGA compatible controller: NVIDIA Corporation GT218 [ION] (rev a2)~~
00:02.0 VGA compatible controller: Intel Corporation Iris Plus Graphics 655 (rev 01)

CPU:
~~model name	: Intel(R) Atom(TM) CPU D525   @ 1.80GHz~~
model name	: Intel(R) Core(TM) i3-8109U CPU @ 3.00GHz

Kodi source code must be placed next to this repository in folder named 'kodi'

Clone from https://github.com/xbmc/xbmc.git

## install-dependencies.sh

Install debian packages

## configure-kod.sh

Configures the kodi build which is important here to run on the device

## build-missing-deps.sh

Builds dependencies which are missing or outdated in debian repository

## build-kodi.sh

Builds kodi

## build-inpustream-adaptive.sh

Clone from https://github.com/peak3d/inputstream.adaptive.git

Then run the script, it creates a zip to the home folder which then can be installed to kodi.
